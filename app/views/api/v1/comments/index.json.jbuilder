json.array! @comments do |comment|
  json.id         comment.id
  json.message    comment.message
  
  # Comment author info
  json.user do
    json.id     comment.user.id
    json.email  comment.user.email
  end
end