json.array! @posts do |post|
  json.id       post.id
  json.title     post.title
  json.content  post.content
  
  json.comments_count   post.comments_count
  
  # Post author info
  json.user do
    json.id     post.user.id
    json.email  post.user.email
  end
  
end