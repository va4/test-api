json.id         @post.id
json.title      @post.title
json.content    @post.content

# Post author info
json.user do
  json.id     @post.user.id
  json.email  @post.user.email
end