class Comment < ActiveRecord::Base
  belongs_to :user
  
  belongs_to :post, 
             :counter_cache => true
  
  validates :user, 
            :presence => true

  validates :post, 
            :presence => true
  
  validates :message,  
            :length => { :minimum => 1 },
            :presence => true
end
