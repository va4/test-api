class User < ActiveRecord::Base
  
  # User role, by deafult is equal to 0
  enum role: { guest: 0, user: 1, admin: 2 }
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # :registerable, :recoverable, :rememberable, :trackable,
  devise :database_authenticatable, :registerable, :validatable

  has_many :posts, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  
  validates :role, 
            :presence => true
end
