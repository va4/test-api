class Post < ActiveRecord::Base  
  belongs_to :user
  
  has_many :comments, 
    :dependent => :destroy

  validates :title, 
    :length => { :minimum => 5 },
    :presence => true
  
  validates :content, 
    :length => { :minimum => 25 },
    :presence => true
end
