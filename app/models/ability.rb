class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # when user not logged in
    
    if user.admin?
      can :manage, :all
    
    elsif user.user?
      can :read, :all
      can :create, :all
      
      # User can update or delete only own records 
      can [:update, :destroy], [Post, Comment], :user_id => user.id
    
    elsif user.guest?
      can :read, :all
    end    

  end
end