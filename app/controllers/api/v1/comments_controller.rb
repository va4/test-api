class Api::V1::CommentsController < ::ApiController
  before_filter :find_post_model
  before_filter :find_model, :only => [:update, :destroy]

  # GET
  # /api/v1/posts/:post_id/comments
  def index
    @comments = @post.comments
      .includes(:user)
      .order("id DESC")
      .page(params[:page])
  end

  # POST
  # /api/v1/posts/:post_id/comments
  def create
    @comment = current_user.comments.create!(comment_params) do |comment|
      comment.post = @post
    end
    
    render :show, :status => :created
  end

  # PUT
  # /api/v1/posts/:post_id/comments/:id
  def update
     @comment.update_attributes!(comment_params)
     head_ok
  end

  # DELETE
  # /api/v1/posts/:post_id/comments/:id
  def destroy
    if @post.destroy 
      head_ok
    else
      render_error(@post.errors)
    end
  end

  private
    
    # We use this model on [create, update, destroy] just to be shure that URL is right
    # and comment belongs to the post with given id, of course we 
    # can find comment by id directly, it would be faster but with potential risks
    def find_post_model
      @post = Post.find(params[:post_id])
    end
    
    def find_model
      @comment = @post.comments.includes(:user).find(params[:id])
    end
    
    def comment_params
      params.require(:comment).permit(:message)
    end
end
  