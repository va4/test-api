class Api::V1::PostsController < ::ApiController
  before_filter :find_model, :only => [:show, :update, :destroy]

  # GET /api/v1/posts
  def index
    @posts = Post.includes(:user).page(params[:page])
  end

  # GET /api/v1/posts/:id
  def show; end

  # POST /api/v1/posts
  def create
    @post = current_user.posts.create!(post_params)
    render :show, :status => :created
  end

  # PUT /api/v1/posts/:id
  def update
     @post.update_attributes!(post_params)
     head_ok
  end

  # DELETE /api/v1/posts/:id
  def destroy
    if @post.destroy 
      head_ok
    else
      render_error(@post.errors)
    end
  end

  private

    def find_model
      @post = Post.includes(:user).find(params[:id])
    end
    
    def post_params
      params.require(:post).permit(:title, :content)
    end
end