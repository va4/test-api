class ApiController < ApplicationController
  respond_to :json

  before_filter :http_authenticate
  load_and_authorize_resource
  
  # API exception handling
  rescue_from Exception, :with => :render_exception
  rescue_from AuthenticationError, :with => :render_authentication_error
  rescue_from CanCan::AccessDenied, :with => :render_access_error
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  rescue_from ActiveRecord::RecordInvalid, :with => :record_invalid
  
  private 

  def http_authenticate
      authenticate_or_request_with_http_basic do |user_name, password|        
        user = User.find_by_email(user_name.to_s)        
        if user && user.valid_password?(password)
          true
        else
          nil # We can raise AuthenticationError here if we need
        end
      end
  end

  def record_not_found(error)
    render_exception(error, 404, false)
  end

  def record_invalid(error)
    error_message = error.is_a?(ActiveRecord::RecordInvalid) ? 
                      error.record.errors.messages : e.message
    
    render_error(error_message, 422)
  end
    
  def render_exception(error, status = 500, backtrace = true)
    if backtrace
      logger.error error.message
      logger.error error.backtrace.join("\n\tfrom ")
    end
    
    render_error(error.message, status)
  end
  
  def render_access_error(error)
    #logger.info "Access Error: #{error.message}"
    
    render_exception(error, 403, false)
  end
  
  def render_authentication_error(error)
    #logger.info "Authentication Error: #{error.message}"
    
    render_exception(error, 401, false)
  end
  
  def render_error(error_message, status = 500)
    render :json => { :error => error_message }, :status => status
  end
  
  def head_ok
    # We setting content type for prevnet 
    # error of some IOS network libraries, when they 
    # got text/json they trying to parse body even 
    # if it empty and gotting exception
    head :ok, content_type: "text/html"
  end
end