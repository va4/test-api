# Helper for user creation with given role
def user_with_role(role)    
  User.find_or_create_by(email: "#{role}@test.com", role: role) do |u|
    u.password = '12345678'
  end
end

# Helper method for Http BAsic Auth
def env_for_user(user)
  env = {}
  env['HTTP_AUTHORIZATION'] = 
    ActionController::HttpAuthentication::Basic.encode_credentials(user.email, '12345678')
  env
end
