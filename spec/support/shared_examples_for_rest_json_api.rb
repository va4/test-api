require 'support/json_helper'
require 'support/auth_helper'

shared_examples_for "index JSON API endpoint" do
  it "return list of objects by GET requst" do
    env = env_for_user(user)
    
    eval "#{http_method.downcase}(path, params, env)"
    expect(response).to have_http_status(200)
    expect(json.count).to eq(requested_objects.count)
    
    # Check for all attributes 
    expect( attributes - json.first.keys).to eq([])
    
    # TODO: chceck attributes valuse
  end
  
  #TODO: Should put here pagination tests and more...
end


shared_examples_for "show JSON API endpoint" do
  it "should return object by GET requst" do
    env = env_for_user(user)

    eval "#{http_method.downcase}(path, params, env)"
    
    # Check http status code
    expect(response).to have_http_status(200)
    
    # Check objetc id
    expect(json['id']).to eq(requested_object.id)
    
    # Check for all attributes 
    expect( attributes - json.keys ).to eq([])
    
    # TODO: chceck attributes valuse
  end
end


shared_examples_for "create JSON API endpoint" do
  it "create object by POST requst" do
    
    env = env_for_user(user)

    eval "#{http_method.downcase}(path, params, env)"

    # Check http status code
    expect(response).to have_http_status(201)
    
    request_params = params.values.first 
    response_json = json
    
    # Check attributes 
    response_json.keys.each do |attr_name|
      if request_params.has_key?(attr_name)
        expect(response_json[attr_name]).to eq request_params[attr_name]
      end
    end

  end
end


shared_examples_for "update JSON API endpoint" do
  it "should update object by PUT requst" do
    
    env = env_for_user(user)
    
    eval "#{http_method.downcase}(path, params, env)"
    
    # Check http status code
    expect(response).to have_http_status(200)
  end
end


shared_examples_for "delete JSON API endpoint" do
  it "delete object by DELETE requst" do
    env = env_for_user(user)
    
    eval "#{http_method.downcase}(path, {}, env)"
    expect(response).to have_http_status(200)
  end
end


shared_examples_for "access denied API endpoint" do
  it "should return http status code: 403" do
    
    env = env_for_user(user)
    
    eval "#{http_method.downcase}(path, params, env)"
    
    expect(response).to have_http_status(403)
    expect(json['error']).to eq("You are not authorized to access this page.")
  end
end


shared_examples_for 'unauthorized API endpoint' do
  it "should return http status code: 401" do
    
    eval "#{http_method.downcase}(path, params, {})"
    expect(response).to have_http_status(401)
  end
end