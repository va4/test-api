FactoryGirl.define do
  factory :user do    
    sequence :email do |n|
      "demouser_#{n}@email.com"
    end
    
    password "truepass"
  end
end
