FactoryGirl.define do
  factory :comment do    
    message Faker::Book.title
    
    association :post, factory: :post
    association :user, factory: :user
  end
end