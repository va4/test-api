FactoryGirl.define do
  factory :post do    
    title Faker::Book.title
    content Faker::Lorem.paragraph(2)
    
    association :user, factory: :user
  end
end