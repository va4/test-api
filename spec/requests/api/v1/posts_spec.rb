require 'rails_helper'
require 'support/shared_examples_for_rest_json_api.rb'


RSpec.describe "Api V1 Posts:", type: :request do
  
  # INDEX POST
  describe "GET - /api/v1/posts" do
    let(:http_method) { "GET" }
    let(:params) { nil }
    
    let!(:requested_objects) { FactoryGirl.create_list(:post, 5) }
    let(:attributes) { ['id', 'title', 'content', 'user'] }
    
    let(:path) { api_v1_posts_path }

    context "for Admin role" do
      let(:user) { user_with_role(:admin) }
      include_examples 'index JSON API endpoint'
    end

    context "for User role" do
      let(:user) { user_with_role(:user) }
      include_examples 'index JSON API endpoint'
    end
    
    context "for Guest role" do
      let(:user) { user_with_role(:guest) }
      include_examples 'index JSON API endpoint'
    end
    
    context "for unauthorized requests" do
       include_examples 'unauthorized API endpoint'
    end
  end
  
  
  # SHOW POST
  describe "GET - /api/v1/posts/:id" do
    let(:http_method) { "GET" }
    let(:params) { nil }
    
    let(:requested_object) { FactoryGirl.create(:post) }
    let(:path) { api_v1_post_path(requested_object) }
    let(:attributes) { ['id', 'title', 'content', 'user'] }
    
    context "for Admin role" do
      let(:user) { user_with_role(:admin) }
      include_examples 'show JSON API endpoint'
    end
    
    context "for User role" do
      let(:user) { user_with_role(:user) }
      include_examples 'show JSON API endpoint'
    end
    
    context "for Guest role" do
      let(:user) { user_with_role(:guest) }
      include_examples 'show JSON API endpoint'
    end
    
    context "for Unauthorized requests" do
      include_examples 'unauthorized API endpoint'
    end
  end
  
  
  # CREATE POST
  describe "POST - /api/v1/posts" do
    let(:http_method) { "POST" }
    let(:path) { api_v1_posts_path }
    
    let(:params) do
      post = {}
      post['title'] = Faker::Book.title
      post['content'] = Faker::Lorem.paragraph(2)
      
      { 'post' => post }
    end
    
    context "for Admin role" do
      let(:user) { user_with_role(:admin) }
      include_examples 'create JSON API endpoint'
    end

    context "for User role" do
      let(:user) { user_with_role(:user) }
      include_examples 'create JSON API endpoint'
    end
    
    context "for Guest role" do
      let(:user) { user_with_role(:guest) }
      include_examples 'access denied API endpoint'
    end
    
    context "for Unauthorized requests" do
      include_examples 'unauthorized API endpoint'
    end
  end
  

  # UPDATE POST
  describe "PUT /api/v1/posts/:id" do
    let(:http_method) { "PUT" }
    
    let(:params) do
      post = {}
      post['title'] = Faker::Book.title
      post['content'] = Faker::Lorem.paragraph(2)
      
      { 'post' => post }
    end
    
    let(:post_object) { FactoryGirl.create(:post) }
    let(:path) { api_v1_post_path(post_object) }
    
    context "for Admin role" do
      let(:user) { user_with_role(:admin) }
      include_examples 'update JSON API endpoint'
    end
    
    context "for User role with Own post" do
      let(:user) { user_with_role(:user) }
      
      let(:post_object) { FactoryGirl.create(:post, user_id: user.id) }
      let(:path) { api_v1_post_path(post_object) }
      
      include_examples 'update JSON API endpoint'
    end
    
    context "for User role and Foreign post" do
      let(:user) { user_with_role(:user) }
      include_examples 'access denied API endpoint'
    end
    
    context "for Guest role" do
      let(:user) { user_with_role(:guest) }
      include_examples 'access denied API endpoint'
    end
    
    context "for Unauthorized requests" do
      include_examples 'unauthorized API endpoint'
    end
  end
  
  
  # DELETE POST
  describe "DELETE /api/v1/posts/:id" do
    let(:http_method) { "DELETE" }
    let(:params) { nil }
    
    let(:post_object) { FactoryGirl.create(:post) }
    let(:path) { api_v1_post_path(post_object) }
    
    context "for Admin role" do
      let(:user) { user_with_role(:admin) }
      include_examples 'delete JSON API endpoint'
    end

    context "for User role with own post" do
      let(:user) { user_with_role(:user) }

      let(:post_object) { FactoryGirl.create(:post, user_id: user.id) }
      let(:path) { api_v1_post_path(post_object) }
      
      include_examples 'delete JSON API endpoint'
    end
    
    context "for User role and foreign post" do
      let(:user) { user_with_role(:user) }
      include_examples 'access denied API endpoint'
    end
    
    context "for Guest role" do
      let(:user) { user_with_role(:guest) }
      include_examples 'access denied API endpoint'
    end
    
    context "for Unauthorized requests" do
      include_examples 'unauthorized API endpoint'
    end  
    
  end
end
