require 'rails_helper'
require 'support/shared_examples_for_rest_json_api.rb'

RSpec.describe "Api V1 Comments", type: :request do


  # INDEX COMMENTS
  describe "GET /api/v1/posts/:id/comments" do
    let(:http_method) { "GET" }
    let(:params) { nil }
    
    let(:post_object) { FactoryGirl.create(:post) }
    let!(:requested_objects) { FactoryGirl.create_list(:comment, 5, post_id: post_object.id) }
    let(:path) { api_v1_post_comments_path(post_object)  }
    
    let(:attributes) { ['id', 'message', 'user'] }
    
    context "for Admin role" do
      let(:user) { user_with_role(:admin) }
      include_examples 'index JSON API endpoint'
    end
    
    context "for User role" do
      let(:user) { user_with_role(:user) }
      include_examples 'index JSON API endpoint'
    end
    
    context "for Guest role" do
      let(:user) { user_with_role(:guest) }
      include_examples 'index JSON API endpoint'
    end
    
    context "for unauthorized requests" do
      include_examples 'unauthorized API endpoint'
    end
  end
  
  
  # CREATE COMMENT
  describe "POST /api/v1/posts/:id/comments" do
    let(:http_method) { "POST" }
    
    let(:post_object) { FactoryGirl.create(:post) }
    let(:path) { api_v1_post_comments_path(post_object) }
    
    let(:params) do 
      {'comment': { 'message': Faker::Lorem.sentence(2) } }
    end
    
    context "for Admin role" do
      let(:user) { user_with_role(:admin) }
      include_examples 'create JSON API endpoint'
    end

    context "for User role" do
       let(:user) { user_with_role(:user) }
       include_examples 'create JSON API endpoint'
    end
    
    context "for Guest role" do
      let(:user) { user_with_role(:guest) }
      include_examples 'access denied API endpoint'
    end
    
    context "for Unauthorized requests" do
      include_examples 'unauthorized API endpoint'
    end
  end
  
  
  # UPDATE COMMENT
  describe "PUT /api/v1/posts/:post_id/comments/:id" do
    let(:http_method) { "PUT" }
    
    let(:post_object) { FactoryGirl.create(:post) }
    let(:comment_object) { FactoryGirl.create(:comment, post: post_object) }
    let(:path) { api_v1_post_comment_path(post_object, comment_object) }
    
    let(:params) do 
      {'comment': { 'message': Faker::Lorem.sentence(2) } }
    end
    
    context "for Admin role" do
      let(:user) { user_with_role(:admin) }
      include_examples 'update JSON API endpoint'
    end
    
    context "for User role and own comment" do
       let(:user) { user_with_role(:user) }
       
       let(:comment_object) { FactoryGirl.create(:comment, post: post_object, user: user ) }
       let(:path) { api_v1_post_comment_path(post_object, comment_object) }
       
       include_examples 'update JSON API endpoint'
    end
    
    context "for User role and foreign comment" do
      let(:user) { user_with_role(:user) }
      include_examples 'access denied API endpoint'
    end
     
    context "for Guest role" do
      let(:user) { user_with_role(:guest) }
      include_examples 'access denied API endpoint'
    end
    
    context "for Unauthorized requests" do      
      include_examples 'unauthorized API endpoint'
    end
  end


  # DELETE COMMENT
  describe "DELETE /api/v1/posts/:post_id/comments/:id" do
    let(:http_method) { "DELETE" }
    let(:params) { nil }
    
    let(:post_object) { FactoryGirl.create(:post) }
    let(:comment_object) { FactoryGirl.create(:comment, post: post_object) }
    let(:path) { api_v1_post_comment_path(post_object, comment_object) }
    
    context "for Admin role" do
      let(:user) { user_with_role(:admin) }
      
      include_examples 'delete JSON API endpoint'
    end
  
    context "for User role and own comment" do
       let(:user) { user_with_role(:user) }
       
       let(:comment_object) { FactoryGirl.create(:comment, post: post_object, user: user ) }
       let(:path) { api_v1_post_comment_path(post_object, comment_object) }
       
       include_examples 'delete JSON API endpoint'
    end
    
    context "for User role and foreign comment" do
       let(:user) { user_with_role(:user) }
       include_examples 'access denied API endpoint'
    end
    
    context "for Guest role" do
      let(:user) { user_with_role(:guest) }
      include_examples 'access denied API endpoint'
    end
    
    context "for Unauthorized requests" do
      include_examples 'unauthorized API endpoint'
    end
  end
  
end
