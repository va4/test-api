Rails.application.routes.draw do
  
  devise_for :users
  
  # API routes  
  namespace :api, :defaults => { :format => :json } do
    namespace :v1 do
      
      resources :posts, except: [:new, :edit] do
        resources :comments, except: [:new, :edit, :show]
      
      end
    end
  end
  
  
  
  root to: "application#index"
  
end
