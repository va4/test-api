# Password for test users (not for production)
password = '12345678'

users = []

# Createing users for each role
users << User.create!(email: 'guest@test.com', 
    password: password)

users << User.create!(email: 'user@test.com', 
    password: password, 
    role: :user)

users << User.create!(email: 'admin@test.com',
    password: password, 
    role: :admin)

users.each do |user|
  
  rand(1..7).times do
    
    # Creating random number of posts 
    post = user.posts.create!(title: Faker::Book.title, 
      content: Faker::Lorem.paragraph(2))
    
    rand(1..5).times do
      # Creating random number of comments for each post
      post.comments.create!(message: Faker::Lorem.sentence(2), user: users.sample)
    end
    
  end
end
